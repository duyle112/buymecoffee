import React from 'react';
import { Row, Col, Button, Layout, Icon } from 'antd';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { pure } from 'recompose';
import { PureRouteComponent } from 'common/view/component';
import { AuthAction } from '../redux';

const AppRow = pure(Row);

const style = { textAlign: "center" }

class Login extends React.Component {
    state = {
        redirectToReferrer: false
    }
    login = () => {
        const { authenticated } = this.props;
        const token = "This is a token"
        authenticated(token);
        this.setState({ redirectToReferrer: true });
        this.props.update_user_info({
            name: 'Administrator'
        });
    }
    shouldComponentUpdate(nextProps, nextState) {
        return nextState.redirectToReferrer !== this.state.redirectToReferrer
    }
    fetchUserInfo = async (token) => {
        const response = await fetch('oauth/oauth/userinfo', {
            headers: {
                Authorization: `Bearer ${this.props.token || token}`
            }
        })
        const userInfo = await response.json()
        this.props.update_user_info(userInfo)
    }

    render() {
        console.log('Login::render')
        const { from } = this.props.location.state || { from: { pathname: '/' } }
        const { redirectToReferrer } = this.state

        if (redirectToReferrer) {
            return <Redirect to={from} />
        }

        return (
            <Row type="flex" align="center">
                <Col style={style}>
                    <p>You must log in to view the page</p>
                    <Button type="error" onClick={this.login}><Icon type="login" />Log In</Button>
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const isAuthenticated = state.auth.isAuthenticated;
    const token = state.auth.token;
    return {
        isAuthenticated,
        token
    }
}


export default connect(mapStateToProps, AuthAction)(Login);
