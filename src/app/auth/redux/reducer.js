const initalState = {
    isAuthenticated: false,
    token: null,
    userInfo: null
}

const AuthReducer = (state=initalState, { type, payload }) => {
    switch(type) {
        case 'AUTHENTICATED':
            return {...state, token: payload, isAuthenticated: true}
        case 'UPDATE_USER_INFO':
            return {...state, userInfo: payload};
        case 'LOGOUT':
            return initalState;
        default:
            return state
    }
}

export default AuthReducer;
