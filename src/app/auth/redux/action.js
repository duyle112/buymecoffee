const authenticated = (token) => {
    return {
        type: 'AUTHENTICATED',
        payload: token
    }
}

const logout = () => {
    return {
        type: 'LOGOUT'
    }
}

const update_user_info = (userInfo) => {
    return {
        type: 'UPDATE_USER_INFO',
        payload: userInfo
    }
}

const AuthAction = {
    authenticated,
    logout,
    update_user_info
}

export default AuthAction;
