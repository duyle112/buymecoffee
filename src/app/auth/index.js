import { Login } from './component';
import { AuthReducer } from './redux';

export default (registry) => {
    console.log('Setup module Auth')
    registry.reducer('auth', AuthReducer)
    registry.route('/login', {
        exact: true,
        component: Login
    })
}
