import authSetup from './auth'
import welcomeSetup from './welcome'

export default {
    authSetup,
    welcomeSetup
}