import React from 'react';
import { Row, Col } from 'antd';
import { pure } from 'recompose';
import { PureRouteComponent } from 'common/view/component';

class Welcome extends PureRouteComponent {
    constructor(props) {
        super(props);
        console.log('Welcome::constructor')
    }
    render() {
        console.log('Welcome::render')
        return (
            <Row type="flex" align="center">
                <Col>
                    <div>Welcome</div>
                </Col>
            </Row>
        );
    }
}

export default pure(Welcome);
