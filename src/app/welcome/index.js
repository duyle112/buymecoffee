import Welcome from './component';

export default (registry) => {
    console.log('Setup module Welcome')
    registry.route('/', {
        isPrivate: true,
        exact: true,
        component: Welcome
    })
}