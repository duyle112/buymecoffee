const logger = console

const registeryFactory = () => {
    const ROUTE = {};
    const REDUCER = {};
    const ACTION = {};

    const route = (key, _route) => {
        logger.info(`Register route [${key}]`);
        if (key in route) {
            logger.warn(`Route [${key}] is already registered`);
        }
        ROUTE[key] = _route;
    };
    const reducer = (key, _reducer) => {
        logger.info(`Register reducer [${key}]`);
        if (key in REDUCER) {
            logger.warn(`Reducer [${key}] is already registered`);
        }
        REDUCER[key] = _reducer;
    };
    const action = (key, _action) => {
        logger.info(`Register action [${key}]`);
        if (key in ACTION) {
            logger.warn(`Action [${key}] is already registered`);
        }
        ACTION[key] = _action;
    };
    return {
        route,
        reducer,
        action,
        getRoute: (key) => ROUTE[key],
        getReducer: (key) => REDUCER[key],
        getAction: (key, sub) => ACTION[key][sub],
        getAllRoute: () => ROUTE,
        getAllReducer: () => REDUCER,
        getAllAction: () => REDUCER,
    };
};

export default registeryFactory();