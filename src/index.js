import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route , Switch } from 'react-router-dom';
import { configStoreSync } from './common/redux';
import { SiteWrapper } from './common/view/layout';
import { PrivateRoute } from './common/view/component';
import AppModule from './app';
import registry from './registry'
import Error404 from './common/view/page/404';
import registerServiceWorker from './util/registerServiceWorker';

import '!style-loader!css-loader!antd/dist/antd.css'; // eslint-disable-line
import '!style-loader!css-loader!./static/style/index.css'; // eslint-disable-line


class RootApplication extends React.Component {
    constructor(props) {
        super(props);
        this.initiate();
    }
    initiate = () => {
        Object.keys(AppModule).forEach(name => {
            const module = AppModule[name]
            module(registry)
        })
        this.store = configStoreSync();
    }
    renderRoute = () => {
        const routes = registry.getAllRoute();
        return Object.keys(routes).map(route => {
            const  { isPrivate, ...props } = routes[route]
            const Component = isPrivate ? PrivateRoute : Route
            return (
                <Component path={route} key={route} {...props} />
            )
        })
    }
    render() {
        return (
            <Provider store={this.store}>
                <Router basename="/">
                    <SiteWrapper siteName="BuyMeACoffee">
                        <Switch>
                            {this.renderRoute()}
                            <Route component={Error404} />
                        </Switch>
                    </SiteWrapper>
                </Router>
            </Provider>
        )
    }
}

ReactDOM.render(<RootApplication />, document.getElementById('root'));
registerServiceWorker();
