import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

let PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuthenticated
        ? <Component {...props} />
        : <Redirect to={{
            pathname: '/login',
            state: { from: props.location }
        }} />
  )} />
)

const mapStateToProps = (state, ownProps) => {
    const isAuthenticated = state.auth.isAuthenticated;
    return {
        isAuthenticated
    }
}

export default connect(mapStateToProps)(PrivateRoute);