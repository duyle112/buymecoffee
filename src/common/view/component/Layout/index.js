import React from 'react';

import styled from 'styled-components';


export const Layout = styled.div`
    min-height: 100vh;
    display: flex;
    flex: auto;
    flex-direction: column;
`;