import NavBar from './NavBar';
import SlideBar from './SlideBar';
import PrivateRoute from './PrivateRoute';
import PureRouteComponent from './PureRouteComponent';
import Footer from './Footer';
import {Layout} from './Layout';

export {
    NavBar,
    SlideBar,
    PrivateRoute,
    Footer,
    PureRouteComponent,
    Layout
};
