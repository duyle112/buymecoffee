import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

export default class AppFooter extends React.PureComponent {
    render() {
    	console.log('Footer::render');
        return (
            <Footer style={{ textAlign: 'center', bottom: -10}}>
                Copyright © 2018 <a href="/"> {this.props.siteName}</a>. All rights reserved.
            </Footer>
        );
    }
}
