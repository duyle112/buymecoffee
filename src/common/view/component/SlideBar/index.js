import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Icon } from 'antd';

const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

class SlideBar extends React.PureComponent {
    constructor(props) {
        super(props);
        this.rootSubmenuKeys = ['sub1', 'sub2', 'sub3', 'sub4', 'sub5']
        this.state = {
            collapsed: false,
            selected: [],
            openKeys: ['sub1'],
        };
    }
    onOpenChange = (openKeys) => {
        const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
          this.setState({ openKeys });
        } else {
          this.setState({
            openKeys: latestOpenKey ? [latestOpenKey] : [],
          });
        }
    }
    onCollapse = (collapsed) => {
        console.log(collapsed);
        this.setState({ collapsed });
    }
    toggle = () => {
        this.setState({
          collapsed: !this.state.collapsed,
        });
    }
    render() {
        global.console.log('SlideBar::render');
        return (
            <Sider
                width={300}
                collapsedWidth={0}
                trigger={null}
                collapsible
                collapsed={this.state.collapsed}
                onCollapse={this.onCollapse}
                theme="light"
            >
                <Icon
                    className="ant-layout-sider-zero-width-trigger"
                    type={this.state.collapsed ? 'filter' : 'filter'}
                    onClick={this.toggle}
                />
                <Menu
                    theme="light"
                    selectedKeys={["sub1"]}
                    openKeys={this.state.openKeys}
                    onOpenChange={this.onOpenChange}
                    mode="inline"
                >
                    <SubMenu
                      key="sub1"
                      title={<span>PCP Region Name</span>}
                    >

                    </SubMenu>
                    <SubMenu
                      key="sub2"
                      title={<span>PCP Name</span>}
                    >

                    </SubMenu>
                    <SubMenu
                      key="sub3"
                      title={<span>PCP Group Name</span>}
                    >

                    </SubMenu>
                    <SubMenu
                      key="sub4"
                      title={<span>Atributed PCP Specialty</span>}
                    >

                    </SubMenu>
                    <SubMenu
                      key="sub5"
                      title={<span>Claim Type</span>}
                    >

                    </SubMenu>
                </Menu>
            </Sider>
        );
    }
}

SlideBar.defaultProps = {
    title: null,
};

SlideBar.propTypes = {
    title: PropTypes.string,
};

export default SlideBar;
