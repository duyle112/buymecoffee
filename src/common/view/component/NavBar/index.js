import React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Avatar } from 'antd';
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu';
import { connect } from 'react-redux';
import style from './style.css';

import '!style-loader!css-loader!rc-menu/assets/index.css'; // eslint-disable-line

const Style = {
    dropDownIcon: {
        paddingRight: 10
    },
    header: { backgroundColor: "#fff", height: 46 },
    menu: { lineHeight: '42px', backgroundColor: "#fff" },
    right: { float: 'right' }
}

class NavBar extends React.PureComponent {
    constructor(props) {
        super(props);
        console.log('NavBar::constructor')
        this.state = {
            siteName: 'Docsign'
        };
    }
    renderUserAccount = () => {
        const { isAuthenticated, userInfo } = this.props;
        const userName = userInfo ? userInfo.name : 'Account'

        if (isAuthenticated) {
            return (
                <SubMenu
                    className={style.profileMenu}
                    title={<div className={style.profileMenuItem}>
                        <span>{userName}</span><Avatar icon="user"/>
                    </div>}
                    key="profile"
                    onClick={this.dropDownHandler}
                >
                    <MenuItem key="account-setting">
                        <Link to="/my-account">
                            <span><Icon type="user" style={Style.dropDownIcon}/>My Account</span>
                        </Link>
                    </MenuItem>
                    <MenuItem key="setting">
                        <Link to="/setting">
                            <span><Icon type="setting" style={Style.dropDownIcon}/>Setting</span>
                        </Link>
                    </MenuItem>
                    <Divider />
                    <MenuItem key="logout">
                        <span><Icon type="logout" style={Style.dropDownIcon}/>Logout</span>
                    </MenuItem>
                </SubMenu>
            )
        } else {
            return (
                <MenuItem key="login" className={style.profileMenu}>
                    <Link to="/login">
                        <span><Icon type="login" style={Style.dropDownIcon}/>Login</span>
                    </Link>
                </MenuItem>
            )
        }
    }
    dropDownHandler = ({ key }) => {
        if (key === 'logout') {
            this.props.logout();
        }
    }
    render() {
        console.log('NavBar::render')
        return (
            <div className={style.appHeader}>
                <div className={style.logo}>{this.props.siteName}</div>
                <Menu
                    prefixCls="ant-menu"
                    mode="horizontal"
                >
                    <MenuItem key="dashboard">
                        <Link to="/"><span>Dashboard</span></Link>
                    </MenuItem>
                   <MenuItem key="blog">
                        <Link to="/blog"><span>Blog</span></Link>
                    </MenuItem>
                   <MenuItem key="news">
                        <Link to="/news"><span>News</span></Link>
                    </MenuItem>
                    <MenuItem key="about">
                        <Link to="/about"><span>About</span></Link>
                    </MenuItem>
                    {this.renderUserAccount()}
                </Menu>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const isAuthenticated = state.auth.isAuthenticated;
    const userInfo = state.auth.userInfo;
    return {
        isAuthenticated,
        userInfo
    }
}

const mapActionToDispatch = {
    logout: () => ({
        type: 'LOGOUT'
    })
}

export default connect(mapStateToProps, mapActionToDispatch)(NavBar);

