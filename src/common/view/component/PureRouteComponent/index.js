import React from 'react';

export default class PureRouteComponent extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		const oldUrl = this.props.location.pathname;
		const newUrl = nextProps.location.pathname;
		// console.log(`${this.constructor.name}::old`, oldUrl);
		// console.log(`${this.constructor.name}::new`, newUrl);
		// console.log(`${this.constructor.name}::shouldComponentUpdate`, !_.isEqual(oldUrl, newUrl))
		return oldUrl !== newUrl;
	}
}
