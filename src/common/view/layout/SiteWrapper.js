import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'common/view/component';
import { NavBar, Footer } from '../component';

class SiteWrapper extends React.Component {
    constructor(props) {
        super(props);
        global.console.log('SiteWrapper::constructor');
        this.state = {};
    }
    render() {
        global.console.log('SiteWrapper::render');
        return (
            <Layout>
                <NavBar siteName={this.props.siteName}/>
                <Layout style={{ minHeight: 280 }}>
                    {this.props.children}
                </Layout>
                <Footer siteName={this.props.siteName}/>
            </Layout>
        );
    }
}

SiteWrapper.defaultProps = {
    title: null,
};

SiteWrapper.propTypes = {
    children: PropTypes.node.isRequired,
    title: PropTypes.string,
};

export default SiteWrapper;
