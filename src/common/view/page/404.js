import React from 'react';
import { Row, Col } from 'antd';
import { PureRouteComponent } from 'common/view/component';

export default class Error404 extends PureRouteComponent {
    constructor(props) {
        super(props);
        console.log('Error404::constructor')
    }
    render() {
    	console.log('Error404::render')
        return (
            <Row type="flex" align="center">
                <Col>
                    <div>404</div>
                </Col>
            </Row>
        );
    }
}
