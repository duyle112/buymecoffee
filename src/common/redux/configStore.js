import { createStore } from 'redux';
import rootReducer from './reducer';


const configStoreSync = () => {
    const store = createStore(
        rootReducer(),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
    return store;
};

export default configStoreSync;
