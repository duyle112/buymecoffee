import { combineReducers } from 'redux';
import registry from '../../registry'


const rootReducer = () => {
    const appReducer = combineReducers({
        ...registry.getAllReducer()
    });

    const _reducer = (state, action) => {
        const { type } = action;
        switch (type) {
        case 'RS':
            console.info('ACTION - RESET');
            return {};
        case 'REHYDRATE':
            console.info('ACTION - REHYDRATING');
            break;
        default:
            console.info('ACTION -', type);
            break;
        }

        return appReducer(state, action);
    };
    return _reducer;
};

export default rootReducer;
