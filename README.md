This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Env
* Node v8.11.3
* npm v5.6.0
* git
* IDE
* nginx
* Postgres DB
* Psql admin
* ...
## Remote Server
* hostname: ec2-34-201-3-13.compute-1.amazonaws.com
* port: 5432
* username: dev
* password: dev
## How to get all grant db
1. Download dump file
3. Create local db
2. run this command: psql -h hostname -p port_number -U username -f your_file your_local_db
## Process
![alternativetext](docs/process.png)

## Class Diagram
![alternativetext](docs/CD.png)

## Use case
![alternativetext](docs/UC.png)

